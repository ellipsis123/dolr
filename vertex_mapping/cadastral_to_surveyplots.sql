-- with parcel_table as(
--     select
--         *
--     from
--         barhanpur_village_parcel2 --change name of the file here
-- ),
-- bad_poly as (
--     select
--         *
--     from
--         parcel_table
--     where
--         st_area(geom) / st_perimeter(geom) < 8 --can be changed
-- ),
-- intersection_data as (
--     select
--         max(st_length(st_intersection(p.geom, q.geom))) as m,
--         p.gid as pgid,
--         p.geom as geom
--     from
--         bad_poly as p
--         inner join parcel_table as q on q.gid not in (
--             select
--                 gid
--             from
--                 bad_poly
--         )
--         and st_intersects(p.geom, q.geom)
--     group by
--         p.gid,
--         p.geom
-- ),
-- union_data as (
--     select
--         pgid,
--         r.gid as rgid,
--         r.geom as geom2
--     from
--         intersection_data,
--         barhanpur_village_parcel2 as q,
--         barhanpur_village_parcel2 as r
--     where
--         intersection_data.pgid = q.gid
--         and st_length(st_intersection(q.geom, r.geom)) = intersection_data.m
-- )
-- update
--     barhanpur_village_parcel2
-- set
--     geom = st_union(geom, p.geom2) --good polygon bad polygon
-- from
--     union_data as p
-- where
--     gid = p.rgid;
-----------------------------------------------------------------------------------
drop table if exists village_parcel_temp;

create table village_parcel_temp as
select
    *
from
    barhanpur_village_parcel2;

--change name of file here 
delete from
    village_parcel_temp
where
    PIN is null;

select
    dropTopology('cadastral_top3');

select
    createTopology('cadastral_top3', 32643);

select
    st_createtopogeo('cadastral_top3', geom)
from
    (
        select
            st_SimplifyPreserveTopology(st_collect(st_force2d(geom)), 0) as geom
        from
            village_parcel_temp
    ) as foo;

drop schema if exists cleaning cascade;

create schema cleaning;

create table cleaning.edge as
select
    *
from
    cadastral_top3.edge;

create table cleaning.node as
select
    *
from
    cadastral_top3.node;

alter table
    cleaning.edge
add
    column start_node2_geom geometry(Point, 32643),
add
    column end_node2_geom geometry(Point, 32643);

---------------------query to find next vertex to every start_node and end_node
update
    cleaning.edge
set
    start_node2_geom = subq.geom
from
    (
        select
            (dp).geom as geom,
            edge_id
        from
            (
                select
                    st_dumpPoints(geom) as dp,
                    cleaning.edge.edge_id as edge_id
                from
                    cleaning.edge
            ) as foo
        where
            (dp).path [1] = 2
    ) as subq
where
    cleaning.edge.edge_id = subq.edge_id;

update
    cleaning.edge
set
    end_node2_geom = subq.geom
from
    (
        select
            (dp).geom as geom,
            edge_id
        from
            (
                select
                    st_dumpPoints(geom) as dp,
                    cleaning.edge.edge_id as edge_id
                from
                    cleaning.edge
            ) as foo
        where
            (dp).path [1] = (
                select
                    st_npoints(geom) -1
                from
                    cleaning.edge
                where
                    cleaning.edge.edge_id = foo.edge_id
            )
    ) as subq
where
    cleaning.edge.edge_id = subq.edge_id;

with two_points as (
    with neigh as (
        select
            count(edge_id),
            node_id
        from
            cleaning.edge as p,
            cleaning.node
        where
            start_node = node_id
            or end_node = node_id
        group by
            node_id
    )
    select
        r.node_id
    from
        cleaning.node as r,
        neigh
    where
        r.node_id = neigh.node_id
        and (neigh.count = 2)
)
delete from
    cleaning.edge
where
    start_node in (
        select
            node_id
        from
            two_points
    )
    or end_node in (
        select
            node_id
        from
            two_points
    );

---- code for removing t's starts here
with tpoints as (
    with neigh as (
        select
            count(edge_id),
            node_id
        from
            cleaning.edge as p,
            cleaning.node
        where
            start_node = node_id
            or end_node = node_id
        group by
            node_id
    )
    select
        r.node_id
    from
        cleaning.node as r,
        neigh
    where
        r.node_id = neigh.node_id
        and neigh.count = 3
),
tbegin as (
    select
        p.*
    from
        cleaning.edge as p
        inner join tpoints on start_node = tpoints.node_id
),
tend as (
    select
        p.*
    from
        cleaning.edge as p
        inner join tpoints on end_node = tpoints.node_id
),
t_start_end as (
    select
        tbegin.*
    from
        tbegin
        inner join tend on tbegin.edge_id = tend.edge_id
),
start_side as (
    select
        q.edge_id,
        q.start_node,
        r.start_node2_geom as p1,
        t.start_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.start_node = q.start_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.start_node = q.start_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.start_node,
        r.start_node2_geom as p1,
        t.end_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.start_node = q.start_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.end_node = q.start_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.start_node,
        r.end_node2_geom as p1,
        t.end_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.end_node = q.start_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.end_node = q.start_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.start_node,
        r.end_node2_geom as p1,
        t.start_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.end_node = q.start_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.start_node = q.start_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
),
end_side as (
    select
        q.edge_id,
        q.end_node,
        r.start_node2_geom as p1,
        t.start_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.start_node = q.end_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.start_node = q.end_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.end_node,
        r.start_node2_geom as p1,
        t.end_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.start_node = q.end_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.end_node = q.end_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.end_node,
        r.end_node2_geom as p1,
        t.end_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.end_node = q.end_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.end_node = q.end_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
    union
    select
        q.edge_id,
        q.end_node,
        r.end_node2_geom as p1,
        t.start_node2_geom as p2
    from
        cleaning.edge as r
        inner join t_start_end as q on (
            (
                r.end_node = q.end_node
            )
            and r.edge_id != q.edge_id
        )
        inner join cleaning.edge as t on (
            (
                t.start_node = q.end_node
            )
            and t.edge_id != q.edge_id
        )
    where
        t.edge_id != r.edge_id
)
delete from
    cleaning.edge
where
    edge_id in (
        (
            select
                a.edge_id
            from
                start_side as a
            where
                (
                    degrees(
                        st_angle(
                            a.p1,
                            (
                                select
                                    geom
                                from
                                    cleaning.node
                                where
                                    node_id = a.start_node
                            ),
                            a.p2
                        )
                    ) < 200
                    and degrees(
                        st_angle(
                            a.p1,
                            (
                                select
                                    geom
                                from
                                    cleaning.node
                                where
                                    node_id = a.start_node
                            ),
                            a.p2
                        )
                    ) > 160
                )
        )
        intersect
        (
            select
                a.edge_id
            from
                end_side as a
            where
                (
                    degrees(
                        st_angle(
                            a.p1,
                            (
                                select
                                    geom
                                from
                                    cleaning.node
                                where
                                    node_id = a.end_node
                            ),
                            a.p2
                        )
                    ) < 200
                    and degrees(
                        st_angle(
                            a.p1,
                            (
                                select
                                    geom
                                from
                                    cleaning.node
                                where
                                    node_id = a.end_node
                            ),
                            a.p2
                        )
                    ) > 160
                )
        )
    );

-- -----------------------------code for removing t's ends here
---------- query to remove bad vertices and edges
with two_points as (
    with neigh as (
        select
            count(edge_id),
            node_id
        from
            cleaning.edge as p,
            cleaning.node
        where
            start_node = node_id
            or end_node = node_id
        group by
            node_id
    )
    select
        r.node_id
    from
        cleaning.node as r,
        neigh
    where
        r.node_id = neigh.node_id
        and (neigh.count = 0)
)
delete from
    cleaning.edge
where
    start_node in (
        select
            node_id
        from
            two_points
    )
    or end_node in (
        select
            node_id
        from
            two_points
    );

with two_points as (
    with neigh as (
        select
            count(edge_id),
            node_id
        from
            cleaning.edge as p,
            cleaning.node
        where
            start_node = node_id
            or end_node = node_id
        group by
            node_id
    )
    select
        r.node_id
    from
        cleaning.node as r,
        neigh
    where
        r.node_id = neigh.node_id
        and (neigh.count <= 2)
)
delete from
    cleaning.node
where
    node_id in (
        select
            node_id
        from
            two_points
    );

-- --------------------------------------------st_modedgeheal()
drop table if exists village_parcel_temp;
create table village_parcel_temp as
select
    *
from
    cleaning.edge;
--change name of file here 
delete from
    village_parcel_temp
where
    PIN is null;
select
    dropTopology('cadastral_top4');
select
    createTopology('cadastral_top4', 32643);
select
    st_createtopogeo('cadastral_top4', geom)
from
    (
        select
            st_SimplifyPreserveTopology(st_collect(st_force2d(geom)), 0) as geom
        from
            village_parcel_temp --change the name of the file here
    ) as foo;
---------------------------------------------
-- drop table if exists temp;
-- create table temp as with two_points as (
--     with neigh as (
--         select
--             count(edge_id),
--             node_id
--         from
--             cadastral_top4.edge as p,
--             cadastral_top4.node
--         where
--             start_node = node_id
--             or end_node = node_id
--         group by
--             node_id
--     )
--     select
--         r.node_id,
--         r.geom
--     from
--         cadastral_top4.node as r,
--         neigh
--     where
--         r.node_id = neigh.node_id
--         and (neigh.count = 2)
-- )
-- select
--     p.edge_id as e1,
--     q.edge_id as e2
-- from
--     cadastral_top4.edge as p,
--     cadastral_top4.edge as q
-- where
--     (
--         -- (p.start_node in (select node_id from two_points) and 
--         -- q.end_node in (select node_id from two_points))
--         -- and 
--         p.edge_id != q.edge_id
--         and st_intersection(p.geom, q.geom) in (
--             select
--                 geom
--             from
--                 two_points
--         )
--     );