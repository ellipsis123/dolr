#!/bin/bash

#use this file in the folder which contains all tippan .shp files
# / whose subfolder contains all tippan .shp files

#Change the srid as required
#cadastral data has srid 32643. 
#farmplots have 32644 srid
#tippans have srid 4326
load_data(){
    DATABASE_NAME="$1"
    TABLE_NAME="$2"
    shapefile_location="$3"
    shp2pgsql -I -s 4326 "$shapefile_location" "$TABLE_NAME" | psql -U "postgres" -d "$DATABASE_NAME"
}


load_data_a(){
    DATABASE_NAME="$1"
    TABLE_NAME="$2"
    shapefile_location="$3"
    shp2pgsql -a -s 4326 "$shapefile_location" "$TABLE_NAME" | psql -U "postgres" -d "$DATABASE_NAME"
}
    
a=1
b=1

for i in $(find . -iname '*.shp');do


if [ $a == $b ]
then
	
   load_data "govt" "gavha_tippan" $i
else
	
   load_data_a "govt" "gavha_tippan" $i
fi
let "a=a+1" 
done




